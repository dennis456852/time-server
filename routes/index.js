var express = require('express');
var router = express.Router();
const fs = require('fs')
const path = require('path')

/* GET home page. */
router.get('/', function (req, res, next) {
  res.status(200).send(path.resolve('..', 'public', 'index.html'));
});

router.get('/time', function (req, res) {
  fs.readFile(path.resolve(__dirname, '..', 'data', 'date.txt'), function (err, data) {
    if (err) res.status(500).send(`讀取時間失敗`)
    else {
      res.status(200).send(data.toString())
    }
  })
})

router.get('/target', function (req, res) {
  fs.readFile(path.resolve(__dirname, '..', 'data', 'target.json'), function (err, data) {
    if (err) res.status(500).send(`讀取列表失敗`)
    else {
      res.status(200).send(data.toString())
    }
  })
})

router.post('/target', function (req, res) {
  const { host, port, alias } = req.body
  fs.readFile(path.resolve(__dirname, '..', 'data', 'target.json'), function (err, data) {
    if (err) res.status(500).send(`讀取列表失敗`)
    else {
      let targets = JSON.parse(data.toString())

      targets.push({ host: host, port: port, alias: alias })

      fs.writeFile(path.resolve(__dirname, '..', 'data', 'target.json'), JSON.stringify(targets), function (err) {
        if (err) res.status(500).send(`註冊失敗`)
        else {
          res.status(200).send(`註冊成功`)
        }
      })
    }
  })
})

router.delete('/target', function (req, res) {
  const { host, port } = req.body
  fs.readFile(path.resolve(__dirname, '..', 'data', 'target.json'), function (err, data) {
    if (err) res.status(500).send(`讀取列表失敗`)
    else {
      let targets = JSON.parse(data.toString())

      let index = targets.findIndex((element) => {return (element.host == host && element.port == port)})
      targets.splice(index, 1)

      fs.writeFile(path.resolve(__dirname, '..', 'data', 'target.json'), JSON.stringify(targets), function (err) {
        if (err) res.status(500).send(`刪除失敗`)
        else {
          res.status(200).send(`刪除成功`)
        }
      })
    }
  })
})

module.exports = router;
