$(document).ready(function () {
    $.get("/time", function (data, textStatus) {
        $('#now').html(data);
    })

    $.get("/target", function (data, textStatus) {
        let targets = JSON.parse(data)
        for (let i = 0; i < targets.length; i++) {
            $('#tb').append(`<tr>
            <td scope="row">${targets[i].host}</td>
            <td>${targets[i].port}</td>
            <td>${targets[i].alias}</td>
            <td>
            <button type="button" class="close" id="delete${i}" value="${targets[i].host}:${targets[i].port}">
            <span>&times;</span>
          </button>
            </td>
          </tr>`);
          $(`#delete${i}`).click(function (e) {
              e.preventDefault();
              let args = this.value.split(":")
              let data = {
                  host: args[0],
                  port: args[1]
              }
              $.ajax({
                  type: "delete",
                  url: "/target",
                  data: data,
                  success: function (response) {
                      location.reload()
                  }
              });
          });
        }
    })

    $('#submit').click(function (e) {
        e.preventDefault();
        let data = {
            host: $('#host').val(),
            port: $('#port').val(),
            alias: $('#alias').val(),
        }
        $.post("/target", data,
            function (data, textStatus) {
                location.reload()
            }
        );
    });

})